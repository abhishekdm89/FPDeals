package com.deals.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DealsClientTest {
	CalculateDeals calculateDeals;
	DealsClient dealsClient;

	@Before
	public void setUp() {
		dealsClient = new DealsClient();
		
	}

//	@Test
//	public void testCityName() {
//		dealsClient.getInfoByZipCode("30328");
//		String expected = "Atlanta";
//		String actual = calculateDeals.getCity();
//		assertEquals(expected, actual);
//	}

	@Test
	public void testMessageDallas() {
	
		String expected = "5.0";
		String actual = dealsClient.getDiscount("75252");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testMessageOtherCities() {
		
		String expected = "0.0";
		String actual = dealsClient.getDiscount("28226");
		assertEquals(expected, actual);
	}

}
