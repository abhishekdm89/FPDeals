package com.project;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class PlansRESTClient {
	private String REST_SERVICE_URL="http://localhost:8090/T-MobilePlans_REST/cellPhonePlans";
	
	
	public String getPlansInformation() {
		Client client = ClientBuilder.newClient();
		
		WebTarget webTarget = client.target(REST_SERVICE_URL);
		
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		
		String response = invocationBuilder.get(String.class);
		
		return response;
	}
}
