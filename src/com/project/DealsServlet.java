package com.project;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.deals.core.DealsClient;

/**
 * Servlet implementation class DealsServlet
 */
@WebServlet("/DealsServlet")
public class DealsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DealsServlet() {
        super();
        
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String zip = request.getParameter("Search");
		
		DealsClient deals = new DealsClient();
		String discount = "";
		discount = deals.getDiscount(zip);
		JSONObject obj = new JSONObject();
		obj.put("discount", discount);
		obj.put("zip", zip);
		request.setAttribute("DealInfo", obj);
		RequestDispatcher dispatcher =request.getServletContext().getRequestDispatcher("/dealsByZip.jsp");
			dispatcher.forward(request, response);	
		
	}


}
