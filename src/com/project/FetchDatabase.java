package com.project;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.customer.CustomerInfo;


public class FetchDatabase {
	
//	public static void main(String args[]) {
//		List<CustomerInfo> customerList = getDatabaseInfo();
//		System.out.println(customerList.size());
//	}
	
	public static List<CustomerInfo> getDatabaseInfo() {
		Session session = new Configuration()
				.configure("/resources/hibernate.cfg.xml")
				.buildSessionFactory()
				.openSession();
		
		Transaction transaction = session.beginTransaction();
		
		String hql = "FROM com.customer.CustomerInfo";
		Query query = session.createQuery(hql);
		List<CustomerInfo> results = query.list();
		
//		for(CustomerInfo customer : results) {
//			System.out.println(customer.getFirstName() + " " + customer.getLastName());
//		}
		session.close();
		//System.out.println("Extracted Data");
		return results;
	}
}
