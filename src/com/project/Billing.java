package com.project;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.customer.CustomerInfo;
import com.deals.core.CalculateDeals;
import com.deals.core.DealsClient;

public class Billing {
	private Customer customer;
	private double customerBill;
	private Client client;
	private String REST_SERVICE_URL = "http://localhost:8090/FinalProject/submit/plan";
	private CalculateDeals calculateDeals;
	private DealsClient dealsClient;

	public Billing(String firstName, String lastName, String plan, String deviceContract, String addr1, String addr2,
			String city, String state, String zip) {
		this.calculateCustomerBill(plan, deviceContract);
		this.applyDiscount(zip);
		customer = new Customer(firstName, lastName, addr1, addr2, city, state, zip, plan, deviceContract,
				this.customerBill);
		this.updateDatabase();
	}

	public void calculateCustomerBill(String plan, String deviceContract) {
		System.out.println("Calculating Bill");
		client = ClientBuilder.newClient();
		Form form = new Form();
		form.param("typeOfPlan", plan);
		String lines = "";
		if (plan.equalsIgnoreCase("individual")) {
			lines = "1";
		} else
			lines = "4";
		form.param("lines", lines);
		form.param("contractType", deviceContract);
		String bill = client.target(REST_SERVICE_URL).request(MediaType.TEXT_PLAIN)
				.post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED), String.class);
		this.customerBill = Double.parseDouble(bill);
		customerBill = Math.round(customerBill) * 100 / 100.00;

	}

	public void applyDiscount(String zip) {
		double billAmount = 0;
		dealsClient = new DealsClient();
		String message = dealsClient.getDiscount(zip);
		double discount = Double.parseDouble(message);
		billAmount = customerBill * (100 - discount) / 100;

		// if(city.equalsIgnoreCase("atlanta")) {
		// billAmount = customerBill - (10 * customerBill/100);
		// billAmount = Math.round(billAmount) * 100 / 100.00;
		// }
		// else if(city.equalsIgnoreCase("dallas")) {
		// billAmount = customerBill - (5 * customerBill/100);
		// billAmount = Math.round(billAmount) * 100 / 100.00;
		// }
		// else if(city.equalsIgnoreCase("kansas")) {
		// billAmount = customerBill - (2 * customerBill/100);
		// billAmount = Math.round(billAmount) * 100 / 100.00;
		// }
		// else {
		// billAmount = customerBill;
		// }
		customerBill = billAmount;
	}

	public double getCustomerBill() {
		return this.customerBill;
	}

	public void updateDatabase() {
		Session session = new Configuration().configure("/resources/hibernate.cfg.xml").buildSessionFactory()
				.openSession();

		Transaction transaction = session.beginTransaction();
		CustomerInfo customerInfo = new CustomerInfo();
		customerInfo.setFirstName(customer.getFirstName());
		customerInfo.setLastName(customer.getLastName());
		customerInfo.setAddressLine1(customer.getAddressLine1());
		customerInfo.setAddressLine2(customer.getAddressLine2());
		customerInfo.setCity(customer.getCity());
		customerInfo.setState(customer.getState());
		customerInfo.setZip(customer.getZip());
		customerInfo.setPlan(customer.getPlan());
		customerInfo.setDeviceContract(customer.getDeviceContract());
		customerInfo.setCustomerBill(this.getCustomerBill());
		session.persist(customerInfo);

		transaction.commit();

		session.close();

	}
}
