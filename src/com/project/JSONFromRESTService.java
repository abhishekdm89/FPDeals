package com.project;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONFromRESTService {
	private PlansRESTClient cellPhonePlans;
	private JSONArray jsonArray;
	
	public JSONFromRESTService() {
		this.cellPhonePlans = new PlansRESTClient();
		this.getJSONDataFromRestService();
	}
	
	public void getJSONDataFromRestService() {
		String data = this.cellPhonePlans.getPlansInformation();
		this.jsonArray = new JSONArray(data);
		
		for(int i = 0; i< jsonArray.length(); i++) {
			JSONObject obj = jsonArray.getJSONObject(i);
		}
	}
	
	public JSONArray getPlansData() {
		return this.jsonArray;
	}
}
