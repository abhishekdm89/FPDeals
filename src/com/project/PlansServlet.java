package com.project;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;

@WebServlet("/PlansServlet")
public class PlansServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public PlansServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONFromRESTService jsonPlanData = new JSONFromRESTService();
		JSONArray jsonArray = jsonPlanData.getPlansData();
		request.setAttribute("plans_details", jsonArray);
		RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/plans.jsp");
		dispatcher.forward(request, response);
	}
	
	
	//protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//doGet(request, response);
	//}

}
