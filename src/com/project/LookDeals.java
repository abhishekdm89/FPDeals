package com.project;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/LookDeals")
public class LookDeals extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LookDeals() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = "Please enter your zip code to look for deals";
		request.setAttribute("DealsMessage", message);
		RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/deals.jsp");
		dispatcher.forward(request, response);
	}

}
