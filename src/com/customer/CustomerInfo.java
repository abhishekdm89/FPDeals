package com.customer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CustomerRegistration")
public class CustomerInfo {
	@Id
	@GeneratedValue
	private int CustId;
	private String firstName;
	private String lastName;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private String zip;
	private String plan;
	private String deviceContract;
	private double customerBill;
	
	public CustomerInfo(String firstName, String lastName, String addressLine1, String addressLine2, String city, String state, String zip, String plan, String deviceContract) {
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setAddressLine1(addressLine1);
		this.setAddressLine2(addressLine2);
		this.setCity(city);
		this.setState(state);
		this.setZip(zip);
		this.setPlan(plan);
		this.setDeviceContract(deviceContract);
	}

	public CustomerInfo() {
		// TODO Auto-generated constructor stub
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public String getDeviceContract() {
		return deviceContract;
	}

	public void setDeviceContract(String deviceContract) {
		this.deviceContract = deviceContract;
	}

	public double getCustomerBill() {
		return customerBill;
	}

	public void setCustomerBill(double customerBill) {
		this.customerBill = customerBill;
	}
	
	
	
	
	
}
