package com.deals.core;

import javax.xml.bind.JAXBException;

import net.webservicex.GetInfoByZIPResponse;
import net.webservicex.GetInfoByZIPResponse.GetInfoByZIPResult;
import net.webservicex.USZip;
import net.webservicex.USZipSoap;

public class DealsClient {
	CalculateDeals calculateDeals;

//	public static void main(String[] args) {
//		DealsClient client = new DealsClient();
//		String zip = "30328";
//		String message = "";
//		message = client.getDiscount(zip);
//		System.out.println(message);
//
//	}

	public String getDiscount(String zip) {
		String responseData = getInfoByZipCode(zip);
		calculateDeals = new CalculateDeals();
		String city = calculateDeals.extractCity(responseData);
		String message = calculateDeals.dealsByCity(city);
		return message;
	}

	public String getInfoByZipCode(String zipCode) {

		System.out.println("creating xml file");

		WriteToXml writeToXml = new WriteToXml();
		USZipSoap zipResult = new USZip().getUSZipSoap();
		GetInfoByZIPResult result = zipResult.getInfoByZIP(zipCode);
		GetInfoByZIPResponse response = new GetInfoByZIPResponse();
		response.setGetInfoByZIPResult(result);
		String xml = "";
		try {
			xml = writeToXml.toXML(response);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml;
	}

}
