package com.deals.core;

import java.io.File;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class WriteToXml {

	public String toXML(Object jaxb) throws JAXBException {
		System.out.println("Start write to file");
		JAXBContext context = JAXBContext.newInstance(jaxb.getClass());
		Marshaller marshall = context.createMarshaller();
		StringWriter writer=new StringWriter();
		marshall.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshall.marshal(jaxb, writer);
		System.out.println("End write to file");
		return writer.toString();
	}

}
