package com.deals.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalculateDeals {
	public String extractCity(String responseData) {
		String city = "";
		Pattern pattern = Pattern.compile("<CITY>(.*?)</CITY>");
		Matcher matcher = pattern.matcher(responseData);
		if (matcher.find()) {
			city = matcher.group(1);
		}

		return city;
	} 

	public String dealsByCity(String city ) {
		
		String dealsMessage;
		
		if(city.equalsIgnoreCase("Atlanta")) {
			dealsMessage = "10.0";
		} else if (city.equalsIgnoreCase("Dallas")) {
			dealsMessage = "5.0";
		} else if (city.equalsIgnoreCase("Kansas")) {
			dealsMessage = "2.0";
		} else {
			dealsMessage = "0.0";
		}
		
		return dealsMessage;

	}
}
