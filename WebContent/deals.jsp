<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.project.*" %>
<%@page import="org.json.JSONArray" %>
<%@page import="org.json.JSONObject"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Deals</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
		
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/hover.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- pop-up-box -->
<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
<!-- //pop-up-box -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>
	
<body>
<!-- header -->
	<div class="header">
	<div class="w3_agileits_nav">
	<div class="container">
				<div class="w3ls-nav">
					<nav class="navbar navbar-default">
							<div class="navbar-header">
								<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
		          	<a class="navbar-brand" href="index.html"><img src="images/logo.png" alt="Logo" /></a> 
		        </div>
		        <div id="navbar" class="navbar-collapse collapse">
		          <ul class="nav navbar-nav navbar-right">
				<!--   <a  href="#home" class="navbar-brand"><img src="images/logo.png" alt="Logo" /></a>  -->
								
								<li><a href="phones.html">Phones</a></li>
								<li><a href="LookDeals">Deals</a></li>
								<li><a href="PlansServlet" >Plans</a></li>

								
								
								</ul>
							<div class="clearfix"> </div>							
						</div>	
						<div class="container">
			
			<div class="agile_header_grid">
				<div class="w3_agile_logo">
					
				</div>
				<div class="agileits_w3layouts_sign_in">
					<ul>
						
						<li><div class="agileinfo_search">
								<form action="DealsServlet" method="GET">
									<input type="text" name="Search" placeholder="ZipCode here..." required="">
									<input type="submit" value=" ">
								</form>
							</div></li>
							
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
					</nav>		
			</div>

			</div>
		</div>
			</form>
		</div>
	</div>
<!-- //pop-up-box -->	
<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
	$('.popup-with-zoom-anim').magnificPopup({
		type: 'inline',
		fixedContentPos: false,
		fixedBgPos: true,
		overflowY: 'auto',
		closeBtnInside: true,
		preloader: false,
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in'
	});
																	
	});
</script>
<img src="https://www.tmonews.com/wp-content/uploads/2012/11/Screen-Shot-2012-11-22-at-9.27.01-PM-660x448.png" style="float: center; width: 100%; margin-right: 1%;margin-left: 1%; margin-bottom: 0.5em;"> 

 <div id="navbar" class="navbar-collapse collapse">
		          <ul class="nav navbar-nav navbar-center"> </ul>
				<!--   <a  href="#home" class="navbar-brand"><img src="images/logo.png" alt="Logo" /></a>  -->
		<ul>	<li>		
	<br>	
<h2>
  <a href="#">DEALS</a>
  <div>
  <br>
    <p color="Red"><Strong><%String msg = (String)request.getAttribute("DealsMessage"); 
    				out.println(msg);%></Strong></p>
  </div>
</h2>	</li>
<br><br><br>
			
					</ul>
							<div class="clearfix"> </div>							
						</div>	

<!-- footer -->
		<div class="footer">
		<div class="container">
			<h2><a href="index.html">BE IN TOUCH</a></h2>
			
			

			
			<div class="agileits_w3layouts_nav_right">
					<ul>
						<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
						<li><a href="#" class="w3_agile_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			<form action="#" method="post">
				<input type="email" name="email" placeholder="Your email..." required="">
				<input type="submit" value="Subscribe">
			</form>
			
<!-- //footer -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>