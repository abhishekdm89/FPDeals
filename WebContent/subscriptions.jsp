<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.project.*" %>
<%@page import="com.customer.*" %>
<%@page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer Subscriptions</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="T-Mobile web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns " />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/hover.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- pop-up-box -->
<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
<!-- //pop-up-box -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>
<body>
<!-- header -->
	<div class="header">
	<div class="w3_agileits_nav">
	<div class="container">
				<div class="w3ls-nav">
					<nav class="navbar navbar-default">
							<div class="navbar-header">
								<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
		          	<a class="navbar-brand" href="PlansServlet"><img src="images/logo.png" alt="Logo" /></a> 
		        </div>
		        <div id="navbar" class="navbar-collapse collapse">
		          <ul class="nav navbar-nav navbar-right">
				<!--   <a  href="#home" class="navbar-brand"><img src="images/logo.png" alt="Logo" /></a>  -->
								
								<li><a href="phones.html">Phones</a></li>
								<li><a href="LookDeals">Deals</a></li>
								<li><a href="PlansServlet" >Plans</a></li>

								
								
								</ul>
							<div class="clearfix"> </div>							
						</div>	
					</nav>		
			</div>

			</div>
		</div>
	</div>
<!-- //header -->
	<br>
	<h1>Total Phone Plans Subscribed</h1>
	<br>
	<table>
		<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Address</th>
			<th>City</th>
			<th>State</th>
			<th>Zip</th>
			<th>Plan</th>
			<th>Device Contract</th>
			<th>Bill Amount</th>
		</tr>
		<c:forEach var="customer" items="${result }">
		<tr>
			<td>${customer.firstName }</td>
			<td>${customer.lastName }</td>
			<td>${customer.addressLine1 }, ${customer.addressLine2 }</td>
			<td>${customer.city }</td>
			<td>${customer.state }</td>
			<td>${customer.getZip() }</td>
			<td>${customer.plan }</td>
			<td>${customer.deviceContract }</td>
			<td>${customer.customerBill }</td>
		</tr>
		</c:forEach>
	</table>
	
	<br>
	<br>
	<form action="PlansServlet">
		<input type="submit" value="Home"/>
	</form>
</body>
</html>