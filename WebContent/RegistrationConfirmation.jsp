<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.project.*" %>
<%@page import="org.json.JSONArray" %>
<%@page import="org.json.JSONObject"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Confirmation</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
		
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/hover.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- pop-up-box -->
<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
<!-- //pop-up-box -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>
<body>
	<!-- header -->
	<div class="header">
	<div class="w3_agileits_nav">
	<div class="container">
				<div class="w3ls-nav">
					<nav class="navbar navbar-default">
							<div class="navbar-header">
								<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
		          	<a class="navbar-brand" href="PlansServlet"><img src="images/logo.png" alt="Logo" /></a> 
		        </div>
		        <div id="navbar" class="navbar-collapse collapse">
		          <ul class="nav navbar-nav navbar-right">
				<!--   <a  href="#home" class="navbar-brand"><img src="images/logo.png" alt="Logo" /></a>  -->
								
								<li><a href="phones.html">Phones</a></li>
								<li><a href="LookDeals">Deals</a></li>
								<li><a href="PlansServlet" >Plans</a></li>

								
								
								</ul>
							<div class="clearfix"> </div>							
						</div>	
					</nav>		
			</div>

			</div>

		<div class="container">
			</form>
		</div>
	</div>
<!-- //pop-up-box -->	

<p>
<p>
<br>
<br>
<h2>Congratulations! ${firstName}, ${lastName}. You are successfully registered. </h2>
<br>
<h2>Total Bill per billing period: $ ${TotalBill }</h2>
<br>
<br>
<br>
<br>
<br>
<br>
<!-- footer -->
	<div class="footer">
		<div class="container">
			<h2><a href="index.html">BE IN TOUCH</a></h2>
			
			<div class="agileits_w3layouts_nav_right">
					<ul>
						<li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
						<li><a href="#" class="w3_agile_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			<form action="#" method="post">
				<input type="email" name="email" placeholder="Your email..." required="">
				<input type="submit" value="Subscribe">
			</form>
			<div class="agileits_w3layouts_nav">
				<div class="agileits_w3layouts_nav_left">
					<ul>
						
						<li><a href="contact.html">ContactUs</a></li>
						<li><a href="about.html">Support</a></li>
						<li><a href="icons.html">Careers</a></li>
						<li><a href="SubcriptionsServlet">Subscriptions</a></li>
						
					</ul>
				</div>
				<h3>(+1) 123 456 789</h3>
				<div class="clearfix"> </div>
			</div>
			
		</div>
	</div>
<!-- //footer -->
</body>
</html>